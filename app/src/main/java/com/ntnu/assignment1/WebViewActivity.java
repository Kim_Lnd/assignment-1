package com.ntnu.assignment1;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.ViewGroup;
import android.webkit.WebView;
import android.widget.TextView;

public class WebViewActivity extends AppCompatActivity{

    /**
     * Building Web Apps in WebView - developer.android.com
     * https://developer.android.com/guide/webapps/webview.html
     **/
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_web_view);

        WebView myWebView = (WebView) findViewById(R.id.webview_full);
        myWebView.loadUrl("http://www.vg.no");
    }
}
